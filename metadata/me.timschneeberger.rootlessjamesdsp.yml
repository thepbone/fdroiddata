AntiFeatures:
  - NonFreeNet
Categories:
  - Multimedia
  - System
License: GPL-3.0-or-later
AuthorName: Tim Schneeberger
SourceCode: https://github.com/ThePBone/RootlessJamesDSP
IssueTracker: https://github.com/ThePBone/RootlessJamesDSP/issues
Donate: https://github.com/sponsors/ThePBone

Description: |-
    JamesDSP is a system-wide audio processing engine that doesn't need any root access.
    ADB access via a computer is required for the initial setup.

    JamesDSP supports the following audio effects:
    * Limiter control
    * Output gain control
    * Auto dynamic range compressor
    * Dynamic bass boost
    * Interpolating FIR equalizer
    * Arbitrary response equalizer (Graphic EQ)
    * ViPER-DDC
    * Convolver
    * Live-programmable DSP (scripting engine for audio effects)
    * Analog modelling
    * Soundstage wideness
    * Crossfeed
    * Virtual room effect (reverb)

    Limitations:
    * Apps blocking internal audio capture remain unprocessed (e.g., Spotify, Google Chrome)
    * Apps using some types of HW-accelerated playback may cause issues and need to be manually excluded (e.g., some Unity games)
    * Cannot coexist with (some) other audio effect apps (e.g., Wavelet and other apps that make use of the `DynamicsProcessing` Android API)

    Additionally, this app integrates directly with AutoEQ. Using AutoEQ integration, you
    can search and import frequency responses that aim to correct your headphone to a neutral sound.
    Go to 'Arbitrary response equalizer > Magnitude response > AutoEQ profiles' to get started.

    Antifeatures:
    * NonFreeNet: app downloads AutoEQ profiles from github

RepoType: git
Repo: https://github.com/ThePBone/RootlessJamesDSP

Builds:
  - versionName: 1.1.1
    versionCode: 17
    commit: 3b11513f1648f105d50b2822820b48c2224143bf
    subdir: app
    submodules: true
    gradle:
      - RootlessFdroid
    output: build/outputs/apk/rootlessFdroid/release/JamesDSP-v$$VERSION$$-rootless-fdroid-universal-release-unsigned.apk
    prebuild:
      - sdkmanager --install "cmake;3.18.1"
      - sed -i 's/^.*classpath("com\.google\.gms:google-services.*$//' ../build.gradle.kts
      - sed -i 's/^.*classpath("com\.google\.firebase:firebase-crashlytics-gradle.*$//'
        ../build.gradle.kts
      - sed -i 's/^.*id("com\.google\.gms\.google-services").*$//' build.gradle.kts
      - sed -i 's/^.*id("com.google.firebase.crashlytics").*$//' build.gradle.kts
      - sed -i 's/^.*import\ com\.google\.firebase.*$//' build.gradle.kts
      - sed -i '/configure<CrashlyticsExtension>/,+3d' build.gradle.kts
    scandelete:
      - app/src/main/cpp/libjamesdsp
    ndk: r23c

  - versionName: 1.1.3
    versionCode: 19
    commit: 43a74a7938ccc839d3ea59af68df6667bf041fb3
    subdir: app
    submodules: true
    gradle:
      - RootlessFdroid
    output: build/outputs/apk/rootlessFdroid/release/JamesDSP-v$$VERSION$$-rootless-fdroid-universal-release-unsigned.apk
    prebuild:
      - sdkmanager --install "cmake;3.18.1"
      - sed -i 's/^.*classpath("com\.google\.gms:google-services.*$//' ../build.gradle.kts
      - sed -i 's/^.*classpath("com\.google\.firebase:firebase-crashlytics-gradle.*$//'
        ../build.gradle.kts
      - sed -i 's/^.*id("com\.google\.gms\.google-services").*$//' build.gradle.kts
      - sed -i 's/^.*id("com.google.firebase.crashlytics").*$//' build.gradle.kts
      - sed -i 's/^.*import\ com\.google\.firebase.*$//' build.gradle.kts
      - sed -i '/configure<CrashlyticsExtension>/,+3d' build.gradle.kts
    scandelete:
      - app/src/main/cpp/libjamesdsp
    ndk: r23c

MaintainerNotes: |-
    Scanner complains about app/src/main/cpp/libjamesdsp/Main/DSPManager/libs/android-support-v13.jar,
    however it's needed to build DSPManager for libjamesdsp, not libjamesdsp itself, so it's safe to
    remove it, RootlessJamesDSP doesn't need DSPManager from libjamesdsp.

    Upstream provides an fdroid flavor, crashlytics are removed at compile-time,
    libcrashlytics-connector is still included, however crashlytics-related code
    is also removed at compile-time.

    Cmake needs to be installed manually by sdkmanager, otherwise gradle clean fails

AutoUpdateMode: Version
UpdateCheckMode: Tags
UpdateCheckData: buildSrc/src/main/kotlin/AndroidConfig.kt|versionCode\s=\s(\d+)|.|versionName\s=\s"(.*)"
CurrentVersion: 1.1.3
CurrentVersionCode: 19
